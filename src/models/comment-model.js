const { Schema, model } = require('mongoose');

const CommentSchema = new Schema({
  taskId: {
    type: Schema.Types.ObjectId,
    ref: 'Task',
  },
  content: {
    type: String,
    required: true,
  },
}, { versionKey: false });

module.exports = {
  Comment: model('Comment', CommentSchema),
};
