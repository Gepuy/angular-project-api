const { Schema, model } = require('mongoose');

const TaskSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  boardId: {
    type: Schema.Types.ObjectId,
    ref: 'Board',
    required: true,
  },
  status: {
    type: String,
    enum: ['To do', 'In progress', 'Done', 'Archived'],
    default: 'To do',
  },
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'Users',
  },
  imageUrl: {
    type: String,
    required: false,
    default: null,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
}, { versionKey: false });

module.exports = {
  Task: model('Task', TaskSchema),
};
