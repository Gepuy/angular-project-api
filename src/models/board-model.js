const { Schema, model } = require('mongoose');

const BoardSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  created_by: {
    type: Schema.Types.ObjectId,
    ref: 'Users',
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
}, { versionKey: false });

module.exports = {
  Board: model('Board', BoardSchema),
};
