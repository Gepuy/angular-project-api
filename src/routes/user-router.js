const Router = require('express');

const router = new Router();
const userController = require('../controllers/user-controller');

router.get('/me', userController.getProfileInfo);
router.delete('/me', userController.deleteProfile);

module.exports = router;
