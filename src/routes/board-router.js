const Router = require('express');

const router = new Router();
const boardController = require('../controllers/board-controller');

router.get('/', boardController.getUserBoards);
router.get('/:id', boardController.getBoardInfo);
router.post('/', boardController.createBoard);
router.put('/:id', boardController.editBoard);
router.delete('/:id', boardController.deleteBoard);

module.exports = router;
