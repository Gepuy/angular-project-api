const Router = require('express');
const AuthRouter = require('./auth-router');
const UserRouter = require('./user-router');
const BoardRouter = require('./board-router');
const TaskRouter = require('./task-router');

const authMiddleware = require('../middleware/authMiddleware');

const router = new Router();

router.use('/auth', AuthRouter);
router.use('/users', authMiddleware, UserRouter);
router.use('/boards', authMiddleware, BoardRouter);
router.use('/tasks', authMiddleware, TaskRouter);

module.exports = router;
