const Router = require('express');

const router = new Router();
const taskController = require('../controllers/task-controller');

router.get('/', taskController.getAllUserTasks);
router.get('/board/:boardId', taskController.getAllBoardTasks);
router.get('/archived', taskController.getArchivedTasks);
router.get('/:id', taskController.getTask);
router.post('/:boardId', taskController.createTask);
router.put('/:id', taskController.editTask);
router.patch('/:id', taskController.changeTaskStatus);
router.delete('/:id', taskController.deleteTask);
router.get('/:taskId/comment', taskController.getComments);
router.post('/:taskId/comment', taskController.addComment);
router.delete('/:id/comment', taskController.deleteComment);

module.exports = router;
