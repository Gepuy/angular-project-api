const UserNotFound = new Error('User not found');
const UserWithEmailAlreadyExists = new Error('User with this email already exists');
const BoardNotFound = new Error('Board with that id was not found');
const TaskNotFound = new Error('Task was not found');
const CommentNotFound = new Error('Comment was not found');

module.exports = {
  UserNotFound,
  UserWithEmailAlreadyExists,
  BoardNotFound,
  TaskNotFound,
  CommentNotFound,
};
