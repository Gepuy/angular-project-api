/* eslint-disable */
const Board = require('../dao/board');
const ApiError = require("../error/ApiError");

class BoardController {
  async getUserBoards(req, res, next) {
    const user = req.user;
    const boards = await Board.getAll(user.id);

    return res.json({ boards })
  }


  async getBoardInfo(req, res, next) {
    try {
      const board = await Board.getOneById(req.params.id);
      return res.json({ board })
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  async createBoard(req, res, next) {
    try {
      const user = req.user;
      const {name, description} = req.body;
      const board = await Board.create({
        name,
        description,
        created_by: user.id
      })

      return res.json({ board });
    } catch (e) {
      return next(ApiError.badRequest(e.message))
    }
  }

  async editBoard(req, res, next) {
    try {
      if (!req.body.name) {
        return res.status(401).json({message: "Name cannot be null"})
      }
      const board = await Board.updateById(req.params.id, req.body)
      return res.json({ board });
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  async deleteBoard(req, res, next) {
    try {
      await Board.deleteById(req.params.id)
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
    return res.json({ message: 'Board was successfully deleted' });
  }
}

module.exports = new BoardController();
