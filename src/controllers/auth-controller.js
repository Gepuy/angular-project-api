const bcrypt = require('bcryptjs');
const { userJoiSchema } = require('../models/user-model');
const ApiError = require('../error/ApiError');
const User = require('../dao/user');
const generateAccessToken = require('../utils/generateAccessToken');

class AuthController {
  // eslint-disable-next-line class-methods-use-this
  async registration(req, res, next) {
    const { email, nickname, password } = req.body;
    try {
      await userJoiSchema.validateAsync({ email, nickname, password });
      const hashPassword = bcrypt.hashSync(password, 7);
      await User.create(email, nickname, hashPassword);
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
    return res.json({ message: 'Profile created successfully' });
  }

  // eslint-disable-next-line class-methods-use-this
  async login(req, res, next) {
    const { email, password } = req.body;
    try {
      await userJoiSchema.extract(['email']).validateAsync(email);
      await userJoiSchema.extract(['password']).validateAsync(password);
      const user = await User.getByEmail(email);
      const isPassEquals = await bcrypt.compare(password, user.password);
      if (!isPassEquals) {
        return next(ApiError.badRequest('Incorrect password'));
      }
      const token = generateAccessToken(user.id, user.email, user.nickname);
      return res.json({ token });
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }
}

module.exports = new AuthController();
