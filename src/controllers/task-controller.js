/* eslint-disable */
const Task = require('../dao/task');
const Comment = require('../dao/comment');
const ApiError = require("../error/ApiError");
const Board = require("../dao/board");

class TaskController {
  async getAllUserTasks(req, res, next) {
    const user = req.user;
    const tasks = await Task.getAll(user.id);

    return res.json({ tasks });
  }

  async getAllBoardTasks(req, res, next) {
    const {boardId} = req.params;
    const board = await Board.getOneById(boardId);

    const tasks = await Task.getAllByBoardId(boardId);
    return res.json({ boardInfo: {board, tasks} });
  }

  async getArchivedTasks(req, res, next) {
    const userId = req.user.id;
    const archivedTasks = await Task.getArchivedTasks(userId)

    return res.json({ archivedTasks });
  }

  async getTask(req, res, next) {
    try {
      const task = await Task.getOneById(req.params.id);
      return res.json({ task });
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  async createTask(req, res, next) {
    try {
      const {name, status} = req.body;
      const {boardId} = req.params;
      const task = await Task.create({
        name,
        boardId,
        status,
        created_by: req.user.id
      })

      return res.json({ task });
    } catch (e) {
      return next(ApiError.badRequest(e.message))
    }
  }

  async editTask(req, res, next) {
    try {
      const task = await Task.updateById(req.params.id, req.body);
      return res.json({ task });
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  async deleteTask(req, res, next) {
    try {
      await Task.deleteById(req.params.id)
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
    return res.json({ message: 'Task was successfully deleted' });
  }

  async changeTaskStatus(req, res, next) {
    try {
      const { status } = req.body;
      const task = await Task.changeStatus(req.params.id, status);

      return res.json({ task });
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  async getComments(req, res, next) {
    try {
      const { taskId } = req.params;
      const comments = await Comment.getAll(taskId);

      return res.json({comments});
    } catch (e) {
      return next(ApiError.badRequest(e.message))
    }
  }

  async addComment(req, res, next) {
    try {
      const { taskId } = req.params;
      const { content } = req.body;

      const newComment = await Comment.create(taskId, content);
      return res.json({ newComment })
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
  }

  async deleteComment(req, res, next) {
    try {
      await Comment.delete(req.params.id)
    } catch (e) {
      return next(ApiError.badRequest(e.message));
    }
    return res.json({message: 'Comment was successfully deleted'})
  }
}

module.exports = new TaskController();
