const User = require('../dao/user');

class UserController {
  // eslint-disable-next-line class-methods-use-this
  async getProfileInfo(req, res, next) {
    const user = await User.getById(req.user.id);

    res.json({
      user: {
        id: user.id,
        email: user.email,
        nickname: user.nickname,
      },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async deleteProfile(req, res, next) {
    await User.delete(req.user.id);
    return res.json({ message: 'Profile deleted successfully' });
  }
}

module.exports = new UserController();
