const TaskSource = require('./source/mongodb/Task');

class Task {
  constructor(source) {
    this.source = source;
  }

  async getAll(userId) {
    // eslint-disable-next-line no-return-await
    return await this.source.getAll(userId);
  }

  async getAllByBoardId(boardId) {
    // eslint-disable-next-line no-return-await
    return await this.source.getAllByBoardId(boardId);
  }

  async getArchivedTasks(userId) {
    // eslint-disable-next-line no-return-await
    return await this.source.getArchivedTasks(userId);
  }

  async getOneById(taskId) {
    const task = await this.source.getOneById(taskId);
    return task;
  }

  async create(data) {
    const task = await this.source.create(data);
    return task;
  }

  async updateById(id, data) {
    const task = await this.source.updateById(id, data);
    return task;
  }

  async deleteById(id) {
    await this.source.deleteById(id);
  }

  async changeStatus(id, status) {
    const task = await this.source.changeStatus(id, status);
    return task;
  }
}

module.exports = new Task(TaskSource);
