const CommentSource = require('./source/mongodb/Comment');

class Comment {
  constructor(source) {
    this.source = source;
  }

  async getAll(taskId) {
    const comments = await this.source.getAll(taskId);
    return comments;
  }

  async create(taskId, content) {
    const comment = await this.source.create(taskId, content);
    return comment;
  }

  async delete(id) {
    await this.source.delete(id);
  }
}

module.exports = new Comment(CommentSource);
