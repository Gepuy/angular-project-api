/* eslint-disable */
const UserSource = require('./source/mongodb/User')

class User {
  constructor(source) {
    this.source = source;
  }

  async getByEmail(email) {
    return await this.source.getByEmail(email);
  }

  async create(email, nickname, hashPassword) {
    await this.source.create(email, nickname, hashPassword);
  }

  async getById(id) {
    return await this.source.getById(id);
  }

  async delete(id) {
    await this.source.delete(id);
  }

  async update(id, updateConfig) {
    await this.source.update(id, updateConfig);
  }
}

module.exports = new User(UserSource);

