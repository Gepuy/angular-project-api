const { Comment: CommentModel } = require('../../../models/comment-model');
const { CommentNotFound } = require('../../../error/daoError');

class Comment {
  // eslint-disable-next-line class-methods-use-this
  async getAll(taskId) {
    const comments = await CommentModel.find({ taskId });
    return comments;
  }

  // eslint-disable-next-line class-methods-use-this
  async create(taskId, content) {
    // eslint-disable-next-line no-return-await
    return await CommentModel.create({ taskId, content });
  }

  // eslint-disable-next-line class-methods-use-this
  async delete(id) {
    const comment = await CommentModel.findByIdAndDelete(id);
    if (!comment) {
      throw CommentNotFound;
    }
  }
}

module.exports = new Comment();
