/* eslint-disable class-methods-use-this */
const { Task: TaskModel } = require('../../../models/task-model');
const { TaskNotFound } = require('../../../error/daoError');

class Task {
  async getAll(userId) {
    const tasks = await TaskModel.find({ created_by: userId });
    return tasks;
  }

  async getAllByBoardId(boardId) {
    const tasks = await TaskModel.find({ boardId });
    return tasks;
  }

  async getArchivedTasks(userId) {
    const tasks = await TaskModel.find({ created_by: userId, status: 'Archived' });
    return tasks;
  }

  async getOneById(id) {
    const task = await TaskModel.findById(id);
    if (!task) {
      throw TaskNotFound;
    }
    return task;
  }

  async create(data) {
    const task = await TaskModel.create(data);
    return task;
  }

  async updateById(id, data) {
    const task = await TaskModel.findByIdAndUpdate(id, data);
    if (!task) {
      throw TaskNotFound;
    }
  }

  async deleteById(id) {
    const task = await TaskModel.findByIdAndDelete(id);
    if (!task) {
      throw TaskNotFound;
    }
  }

  async changeStatus(id, status) {
    const task = await TaskModel.findByIdAndUpdate(id, { status });
    if (!task) {
      throw TaskNotFound;
    }
    return task;
  }
}

module.exports = new Task();
