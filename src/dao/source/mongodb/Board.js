/* eslint-disable class-methods-use-this */
const { Board: BoardModel } = require('../../../models/board-model');
const { BoardNotFound } = require('../../../error/daoError');

class Board {
  async getAll(userId) {
    const boards = await BoardModel.find({ created_by: userId });
    return boards;
  }

  async getOneById(id) {
    const board = await BoardModel.findById(id);
    if (!board) {
      throw BoardNotFound;
    }
    return board;
  }

  async create(data) {
    const board = await BoardModel.create(data);
    return board;
  }

  async updateById(id, data) {
    const board = await BoardModel.findByIdAndUpdate(id, data);
    if (!board) {
      throw BoardNotFound;
    }
  }

  async deleteById(id) {
    const board = await BoardModel.findByIdAndDelete(id);
    if (!board) {
      throw BoardNotFound;
    }
  }
}

module.exports = new Board();
