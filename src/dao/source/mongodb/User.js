/* eslint-disable */
const { User: UserModel } = require('../../../models/user-model');
const { UserNotFound, UserWithEmailAlreadyExists } = require('../../../error/daoError');

class User {
  async getByEmail(email) {
    const user = await UserModel.findOne({ email });
    if (!user) {
      throw UserNotFound;
    }
    return user;
  }

  async getById(id) {
    const user = await UserModel.findById(id);
    if (!user) {
      throw UserNotFound;
    }
    return user;
  }

  async create(email, nickname, hashPassword) {
    const user = await UserModel.findOne({email});
    if (user) {
      throw UserWithEmailAlreadyExists;
    }
    await UserModel.create({ email, nickname, password: hashPassword });
  }

  async delete(id) {
    await UserModel.findByIdAndDelete(id);
  }

  async update(id, updateConfig) {
    await UserModel.findByIdAndUpdate(id, updateConfig);
  }
}

module.exports = new User();
