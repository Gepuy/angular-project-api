const BoardSource = require('./source/mongodb/Board');

class Board {
  constructor(source) {
    this.source = source;
  }

  async getAll(userId) {
    // eslint-disable-next-line no-return-await
    return await this.source.getAll(userId);
  }

  async getOneById(id) {
    const board = await this.source.getOneById(id);
    return board;
  }

  async create(data) {
    const board = await this.source.create(data);
    return board;
  }

  async updateById(id, data) {
    // eslint-disable-next-line no-return-await
    return await this.source.updateById(id, data);
  }

  async deleteById(id) {
    await this.source.deleteById(id);
  }
}

module.exports = new Board(BoardSource);
