const jwt = require('jsonwebtoken');

// eslint-disable-next-line consistent-return
module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    if (!token) {
      return res.status(403).json({ message: 'User should authorize' });
    }
    req.user = jwt.verify(token, process.env.JWT_SECRET_KEY);
    next();
  } catch (e) {
    console.log(e);
    return res.status(403).json({ message: 'User should authorize' });
  }
};
