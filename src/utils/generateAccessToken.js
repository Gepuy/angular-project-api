const jwt = require('jsonwebtoken');

const generateAccessToken = (id, email, nickname) => {
  const payload = {
    id, email, nickname,
  };
  return jwt.sign(payload, process.env.JWT_SECRET_KEY, { expiresIn: '7d' });
};

module.exports = generateAccessToken;
